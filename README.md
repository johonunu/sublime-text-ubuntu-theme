Sublime Text - Ubuntu Theme
====
Ubuntu theme for Sublime Text. Based on Default theme.

Theme
====
![Ubuntu Theme](https://bitbucket.org/johonunu/sublime-text-ubuntu-theme/raw/b2bb69974a396665856607b769b788fe66bdaa49/screenshot.png)

Instalation
====
Create folder ~/.config/sublime-text/Ubuntu-Theme and extract code in it. Open Sublime text and go to Preferences -> Settings-Default. Change the line "theme": "Default.sublime-theme" to "theme": "Ubuntu.sublime-theme" (line 246).

History
====
28.03.2013 - project started (work in progress)